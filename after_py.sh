#!/bin/bash

# Generate new CAVA config with last pywal color scheme
/usr/bin/wal_cava -c ~/.config/cava/config -G 4

# Replace lockscreen with used wallpaper
lastPywal="$(ls -Art ~/.cache/wal/schemes| tail -n 1)"
wallpaper="$(jq -r '.wallpaper' ~/.cache/wal/schemes/$lastPywal)"
convert $wallpaper -resize 1920x1080  ~/wallpapers/lockscreen.png

# remove last pywal file
rm ~/.cache/wal/schemes/$lastPywal

# Reload i3
i3 reload
i3 restart